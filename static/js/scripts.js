
jQuery(document).ready(function() {
	
    /*
        Fullscreen background
    */
    $.backstretch("/static/img/backgrounds/1.jpg");
    
    /*
        Form validation
    */
    $('.login-form input[type="text"], .login-form input[type="password"], .login-form textarea').on('focus', function() {
    	$(this).removeClass('input-error');
    });
    
    $('#registe_button').on('click', function(e) {
    	var username=$("#form-username").val();
    	var password=$("#form-password").val();
    	console.log(username);
    	console.log(password);
    	$.ajax({
    	    url:"/registe/go",
    	    type:"POST",
    	    dataType:"json",
    	    data:{username:username,password:password},
    	    success:function(dd){
    	        alert(dd.status);
    	    },
    	    error:function(){
    	        alert("请求失败");
    	    }
    	});
    });

    $('#login_button').on('click',function(e){
        var username=$("#form-username1").val();
        var password=$("#form-password1").val();
        var data = {username:username,password:password};
        $.ajax({
            url:"/login/verify",
            type:"POST",
            // [text/htm]
            //[application/json;charset:utf-8;]JSON 是一种轻量级的数据格式，以“键-值”对的方式组织的数据。
            // 这个使用这个类型，需要参数本身就是json格式的数据，参数会被直接放到请求实体里，不进行任何处理。
            // 服务端/客户端会按json格式解析数据
            //默认，form表单提交 [application/x-www-form-urlencoded]
            //多媒体类型 [multipart/form-data]
            contentType:"application/x-www-form-urlencoded",
            dataType:"json",
            data:data,
            success:function(e){
                alert(e.message);
                location.href='/index'
            },
            error:function(){
                alert("请求失败");
            }
        });
    });
    
});
