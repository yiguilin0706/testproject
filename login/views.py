import json

from django.core.exceptions import ValidationError
from django.http import HttpResponse
from django.shortcuts import render
from .models import Userinfo
from .forms import RegistrationForm,LoginForm

from django.contrib.auth.models import User


# Create your views here.
def index(request):
    return render(request, 'index.html')

def login(request):
    return render(request, 'login.html')

def registe(request):
    return render(request, 'registe.html')

def regite_go(request):
    if request.method == 'POST':  # 当提交表单时
        form = RegistrationForm(request.POST)
        try:
            valid = form.is_valid()
            if valid:
                username = form.cleaned_data['username']
                password = form.cleaned_data['password']
                print(username, password)
                # 判断参数中是否含有a和b
                if username and password:
                    user = Userinfo()
                    user.username = username
                    user.pwd = password
                    user.save()
                    result = {"status": "注册成功"}
                    return HttpResponse(json.dumps(result, ensure_ascii=False),
                                        content_type="application/json,charset=utf-8")
                else:
                    result = {"status": "注册失败"}
                    return HttpResponse(json.dumps(result, ensure_ascii=False),
                                        content_type="application/json,charset=utf-8")
            else:
                error_msg_list = []
                for items in form.errors.values():
                    error_msg_list.append(items[0])
                error_msg_str = ''.join(error_msg_list)
                result = {"status": error_msg_str}
                return HttpResponse(json.dumps(result, ensure_ascii=False),
                                    content_type="application/json,charset=utf-8")
        except Exception as e :
            print("错误信息："+str(e))
            result = {"status": "错误信息："+str(e)}
            return HttpResponse(json.dumps(result, ensure_ascii=False),
                                content_type="application/json,charset=utf-8")
        finally:
            print('finally...')
    else:
        result = {"status": "方法错误"}
        return HttpResponse(json.dumps(result, ensure_ascii=False),
                            content_type="application/json,charset=utf-8")
    return render(request,'login.html',{'form':form})

def login_verify(request):
    if request.method == 'POST':
        form = LoginForm(request.POST)
        if form.is_valid():
            username = form.cleaned_data.get("username")
            password = form.cleaned_data.get("password")
            password1 = Userinfo.objects.get(username=username).pwd
            if password == password1:
                result= {"message":"登录成功"}
                return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json,charset=utf-8")
            else:
                result = {"message":"密码错误"}
                return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json,charset=utf-8")
        else:
            result = {"message":"参数错误"}
            return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json,charset=utf-8")
    else:
        result = {"message":"请求错误"}
        return HttpResponse(json.dumps(result, ensure_ascii=False), content_type="application/json,charset=utf-8")
    return render(request, 'index.html')





